#!/bin/bash

VOLUME_HOME="/var/lib/mysql"
LOG="/var/log/mysql.err"
LOAD_DIR="/load.d"


StartMariaDB ()
{
    /usr/bin/mysqld_safe > /dev/null 2>&1 &

    # Time out in 1 minute
    LOOP_LIMIT=13
    for (( i=0 ; ; i++ )); do
        if [ ${i} -eq ${LOOP_LIMIT} ]; then
            echo "Time out. Error log is shown as below:"
            tail -n 100 ${LOG}
            exit 1
        fi
        echo "=> Waiting for confirmation of MariaDB service startup, trying ${i}/${LOOP_LIMIT} ..."
        sleep 5
        mysql -uroot -e "status" > /dev/null 2>&1 && break
    done
}

CreateMariaDBUser()
{
	_word=$( [ ${MARIADB_PASS} ] && echo "preset" || echo "random" )
	echo "=> Creating MariaDB user ${MARIADB_USER} with ${_word} password"

	ACTUAL_PASS=${MARIADB_PASS:-$(pwgen -s 12 1)}
	mysql -uroot -e "CREATE USER '${MARIADB_USER}'@'%' IDENTIFIED BY '$ACTUAL_PASS'"
	mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '${MARIADB_USER}'@'%' WITH GRANT OPTION"

	echo "=> Done!"

	echo "===================================================================="
	echo "You can now connect to this MariaDB Server using:"
	echo ""
	echo "    mysql -u$MARIADB_USER -p$ACTUAL_PASS -h<host> -P<port>"
	echo ""
	echo "Please remember to change the above password as soon as possible!"
	echo "MariaDB user 'root' has no password but only allows local connections"
	echo "====================================================================="

}

ImportSql()
{
    echo "=> About to import sql databases supplied in $LOAD_DIR "

    SQL_DUMPS="$(ls $LOAD_DIR)"
    if [ -n "$SQL_DUMPS" ]; then
        for f in $SQL_DUMPS; do
            DBNAME=$f
            echo "---$f---"
            echo "=> Creating database ${DBNAME}"
            mysql -uroot -e "CREATE DATABASE ${DBNAME}"
            echo "=> Loading database ${DBNAME}"
            mysql -uroot ${DBNAME} < ${LOAD_DIR}/${f}
        done
    else
        echo "=> $LOAD_DIR is empty -> not importing"
    fi
}

if [[ ! -d $VOLUME_HOME/mysql ]]; then
    echo "=> An empty or uninitialized MariaDB volume is detected in $VOLUME_HOME"
    echo "=> Installing MariaDB ..."
    mysql_install_db > /dev/null 2>&1
    echo "=> Done!"

    echo "=> Creating admin user!"
	if [ "$MARIADB_PASS" = "**Random**" ]; then
	    unset MARIADB_PASS
	fi


	StartMariaDB
    CreateMariaDBUser
    if [ -d ${LOAD_DIR} ]; then
        ImportSql
    else
        echo "=> $LOAD_DIR does not exist -> not importing"
    fi
	mysqladmin -uroot shutdown
else
    echo "=> Using an existing volume of MariaDB"
fi


tail -F $LOG &
exec mysqld_safe
