# Another Docker MariaDB image

## Usage

```
# random admin password:
$ docker run -d t0mk/mariadb
59..
$ docker logs 59

# chosen password:
$ docker run -d -e MARIADB_PASS=fsdfsdf t0mk/mariadb
```

## Import

In order to import mysql dump "example.sql" to table "example", you should:

```
$ mv example.sql /tmp/load.d/example
$ docker run -d -v /tmp/load.d:/load.d t0mk/mariadb
86...
$ docker logs 86
[...]
=> About to import sql databases supplied in /load.d
---certeum---
=> Creating database example
=> Loading database exmaple
```


